# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import base64
import copy
import datetime
import re
import StringIO
from decimal import *
from zipfile import ZipFile, ZipInfo
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard
from trytond.pyson import Equal, Eval
from trytond.backend import TableHandler
from trytond.transaction import Transaction
from trytond.pool import Pool


_EXPORT_RANGE = [
    ('no_export', 'No Export'),
    ('changes', 'Changes'),
    ('complete', 'Complete')
    ]

_DATEV_GENERAL = {
    'encoding': 'windows-1252',
    'separator': ';',
    'date_format': '%d%m', #TTMM
    'sign': '',
    'decimal_point': ',',
    'thousands_sep': '.',
    }

_DATEV_FIELD_TYPES = {
    'A': lambda value: isinstance(value, (str, unicode)),
    'N': lambda value: isinstance(value, (int, long)),
    'D': lambda value: isinstance(value, datetime.date),
    'C': lambda value: isinstance(value, Decimal),
    }


class ExportDatev(ModelSQL, ModelView):
    'Export Datev'
    _name = 'account.export.datev'
    _description = __doc__
    _rec_name = 'date'

    company = fields.Many2One('company.company', 'Company', required=True,
            readonly=True)
    date = fields.DateTime('Export Date', required=True, readonly=True)
    file_cache = fields.Binary('Export File', readonly=True)
    range_party = fields.Selection(_EXPORT_RANGE, 'Export Range Party',
            required=True, readonly=True)
    range_account = fields.Selection(_EXPORT_RANGE, 'Export Range Account',
            required=True, readonly=True)
    range_move = fields.Selection(_EXPORT_RANGE, 'Export Range Move',
            required=True, readonly=True)
    move_periods = fields.Many2Many('account.export.datev-account.period',
            'export_datev', 'period', 'Periods',
            domain=[('fiscalyear.company', '=', Eval('company'))],
            states={
                'invisible': Equal(Eval('range_move'), 'no_export')
                }, depends=['company', 'range_move'], readonly=True)
    move_journals = fields.Many2Many('account.export.datev-account.journal',
            'export_datev', 'journal', 'Journals',
            states={
                'invisible': Equal(Eval('range_move'), 'no_export')
                }, depends=['range_move'], readonly=True)
    state = fields.Selection([
            ('unknown', 'Unknown'),
            ('failed', 'Failed'),
            ('successful', 'Successful')
            ], 'State', required=True, readonly=True)
    include_opening_balance_moves = fields.Boolean(
            'Include Opening Balance Moves', readonly=True)

    def __init__(self):
        super(ExportDatev, self).__init__()
        self._order.insert(0, ('date', 'DESC'))
        self._error_messages.update({
            'wrong_field_type': 'Programming Error! ' \
                    'Tryton got a wrong field type for datev export!',
            'required_field_missing': 'The export line cannot be created! ' \
                    'There is missing a value for a field that is required!',
            'field_value_too_long': 'The value of field "%s" is too long and ' \
                    'there is not cut action defined for this field. Please ' \
                    'contact your administrator.',
            })

    def init(self, module_name):
        super(ExportDatev, self).init(module_name)
        cursor = Transaction().cursor
        table = TableHandler(cursor, self, module_name)

        # Migration from 2.0 file renamed into file_cache
        # to remove base64 encoding

        if (table.column_exist('file')
                and table.column_exist('file_cache')):
            cursor.execute('SELECT id, file '
                'FROM "' + self._table + '"')
            for file_id, report in cursor.fetchall():
                if report:
                    report = buffer(base64.decodestring(str(report)))
                    cursor.execute('UPDATE "' + self._table + '" '
                        'SET file_cache = %s '
                        'WHERE id = %s', (report, file_id))
                table.drop_column('file')

    def default_state(self):
        return 'unknown'

    def _create_export_line(self, fields, values):
        vals = values.copy()
        res = ''
        field_keys = fields.keys()
        field_keys.sort()
        for key in field_keys:
            field_name = fields[key]['name']
            if vals.get(field_name):
                if not _DATEV_FIELD_TYPES[fields[key]['type']](vals[field_name]):
                    self.raise_user_error('wrong_field_type')
                res += self._format(fields[key],
                        vals[field_name])
                del vals[field_name]
            elif fields[key].get('required', False) == True:
                self.raise_user_error('required_field_missing')
            res += _DATEV_GENERAL['separator']
            if vals == {}:
                break
        return res

    def _format(self, field, value):
        res = ''
        if field.get('type') == 'C':
            res = unicode(self._format_currency_datev(value))
        elif field.get('type') == 'D':
            res = unicode(value.strftime(_DATEV_GENERAL['date_format']))
        else:
            if isinstance(value, int):
                value = unicode(value)
            res = value
        if field.get('length') and len(res) > field['length']:
            if not field.get('cut_action'):
                self.raise_user_error('field_value_too_long',
                        (field['name'],))
            res = self._set_length(field, res)
        return res

    def _format_currency_datev(self, amount):
        amount = "%.2f" % amount
        res = amount.replace('.', _DATEV_GENERAL['decimal_point'])
        res = res + _DATEV_GENERAL['sign']
        return res

    def _set_length(self, field, value):
        res = value
        if field['cut_action'] == 'cut_right':
            res = value[:field['length']]
        elif field['cut_action'] == 'cut_left':
            res = value[-field['length']:]
        elif field['cut_action'] == 'cut_street':
            number_pattern = re.compile('\d+[\w-]*')
            number = number_pattern.search(value)
            street_pattern = re.compile('[\w-]*.?\s')
            street = street_pattern.search(value)
            if number and street:
                street_new = street.group()
                number_new = number.group()
                res = street_new[:field['length'] - len(number_new)-1] + \
                        ' ' + number_new
            else:
                res = value[:field['length']/2] + value[-field['length']/2:]
        return res

ExportDatev()


class ExportDatevJournal(ModelSQL):
    'Export Datev - Journal'
    _name = 'account.export.datev-account.journal'
    _description = __doc__

    export_datev = fields.Many2One('account.export.datev', 'Export Datev',
            required=True, ondelete='CASCADE')
    journal = fields.Many2One('account.journal', 'Journal', required=True,
            ondelete='RESTRICT')

ExportDatevJournal()


class ExportDatevPeriod(ModelSQL):
    'Export Datev - Period'
    _name = 'account.export.datev-account.period'
    _description = __doc__

    export_datev = fields.Many2One('account.export.datev', 'Export Datev',
            required=True, ondelete='CASCADE')
    period = fields.Many2One('account.period', 'Period', required=True,
            ondelete='RESTRICT')

ExportDatevPeriod()


class ExportInit(ModelView):
    _name = 'account.export.init'

    def __init__(self):
        super(ExportInit, self).__init__()
        self.type = copy.copy(self.type)
        if ('datev', 'Datev') not in self.type.selection:
            self.type.selection += [('datev', 'Datev')]
        self._reset_columns()

    def default_type(self):
        return 'datev'

ExportInit()


class ExportDatevWarning(ModelView):
    'Export Datev Warning'
    _name = 'account.export.datev.warning'
    _description = __doc__

ExportDatevWarning()


class ExportDatevInit(ModelView):
    'Export Datev Init'
    _name = 'account.export.datev.init'
    _description = __doc__

    company = fields.Many2One('company.company', 'Company', required=True)
    account = fields.Selection(_EXPORT_RANGE, 'Export account names',
            required=True)
    party = fields.Selection(_EXPORT_RANGE, 'Export party master data',
            required=True)
    move = fields.Selection(_EXPORT_RANGE, 'Export moves',
            required=True)
    move_periods = fields.Many2Many('account.period', None, None, 'Periods',
            domain=[('fiscalyear.company', '=', Eval('company'))],
            states={
                'invisible': Equal(Eval('move'), 'no_export')
                }, depends=['company', 'move'], required=True)
    move_journals = fields.Many2Many('account.journal', None, None, 'Journals',
            states={
                'invisible': Equal(Eval('move'), 'no_export')
                }, depends=['move'])
    include_opening_balance_moves = fields.Boolean(
            'Include Opening Balance Moves',
            states={
                'invisible': Equal(Eval('move'), 'no_export')
            }, depends=['move'])

    def default_company(self):
        return Transaction().context.get('company', False)

    def default_account(self):
        return 'changes'

    def default_party(self):
        return 'changes'

    def default_move(self):
        return 'changes'

ExportDatevInit()


class Export(Wizard):
    _name = 'account.export'

    def __init__(self):
        super(Export, self).__init__()
        self.states = copy.copy(self.states)
        self.states.update({
            'datev': {
                'result': {
                    'type': 'form',
                    'object': 'account.export.datev.init',
                    'state': [
                        ('end', 'Cancel', 'tryton-cancel'),
                        ('datev_warning', 'Ok', 'tryton-ok', True),
                    ],
                },
            },
            'datev_warning': {
                'actions': ['_action_save_data',],
                'result': {
                    'type': 'form',
                    'object': 'account.export.datev.warning',
                    'state': [
                        ('end', 'No', 'tryton-cancel'),
                        ('datev_export', 'Yes', 'tryton-ok', True),
                    ],
                },
            },
            'datev_export': {
                'result': {
                    'type': 'action',
                    'action': '_action_export_datev',
                    'state': 'end',
                },
            },
        })

    def _action_save_data(self, data):
        data['form_init'] = data['form']
        return {}

    def _action_export_datev(self, data):
        pool = Pool()
        party_obj = pool.get('party.party')
        account_obj = pool.get('account.account')
        move_datev_obj = pool.get('account.move.datev')
        export_datev_obj = pool.get('account.export.datev')
        model_data_obj = pool.get('ir.model.data')
        act_window_obj = pool.get('ir.action.act_window')
        company_obj = pool.get('company.company')

        vals = self._get_export_vals(data)
        datev_export_id = export_datev_obj.create(vals)
        datev_export = export_datev_obj.browse(datev_export_id)
        res = {}
        if data['form_init']['party']!='no_export':
            range = data['form_init']['party']
            res.update(party_obj.export_datev(datev_export,
                    range=range))

        if data['form_init']['account']!='no_export':
            range = data['form_init']['account']
            res.update(account_obj.export_datev(datev_export,
                    range=range))

        if data['form_init']['move']!='no_export':
            journal_ids = (data['form_init']['move_journals'] and
                data['form_init']['move_journals'][0][1] or False)
            period_ids = (data['form_init']['move_periods'] and
                data['form_init']['move_periods'][0][1] or False)
            range = data['form_init']['move']
            res.update(move_datev_obj.export_datev(datev_export,
                    period_ids=period_ids, journal_ids=journal_ids,
                    range=range))
        if res:
            company = company_obj.browse(
                    data['form_init']['company'])
            export_file = StringIO.StringIO()
            zip_file = ZipFile(export_file, 'w')
            for file in res:
                info = ZipInfo(str(file))
                info.external_attr = 0777 << 16L
                zip_file.writestr(info, res[file].getvalue())
            zip_file.close()
            data = {
                'file_cache': export_file.getvalue(),
                'state': 'successful'
                }
            export_datev_obj.write(datev_export_id, data)
            export_file.close()

        model_data_ids = model_data_obj.search([
                ('fs_id', '=', 'act_export_datev_view'),
                ('module', '=', 'account_timeline_datev'),
                ('inherit', '=', False),
                ], limit=1)
        model_data = model_data_obj.browse(model_data_ids[0])
        res = act_window_obj.read(model_data.db_id)
        res['res_id'] = datev_export_id
        res['views'].reverse()
        return res

    def _get_export_vals(self, data):
        return {
            'company': data['form_init']['company'],
            'range_party': data['form_init']['party'],
            'range_account': data['form_init']['account'],
            'range_move': data['form_init']['move'],
            'move_journals': data['form_init']['move_journals'],
            'move_periods': data['form_init']['move_periods'],
            'include_opening_balance_moves': data['form_init'][
                'include_opening_balance_moves'],
            'date': datetime.datetime.now(),
            'state': 'failed'
            }

Export()
