# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import re
import copy
import datetime
import StringIO
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard
from trytond.pyson import Equal, Eval, Not, Bool, Or
from trytond.transaction import Transaction
from trytond.pool import Pool


_DATEV_ACCOUNT_FIELDS = {
    1: {
        'name': 'Kontonummer',
        'type': 'N',
        'length': 9,
        'required': True
        },
    2: {
        'name': 'Kontobeschriftung',
        'type': 'A',
        'length': 40,
        'required': True,
        'cut_action': 'cut_right'
        }
    }


class TemplateCollection(ModelSQL, ModelView):
    'Account Template Datev Collection'
    _name = 'account.account.template.datev.collection'
    _description = __doc__

    name = fields.Char('Name', required=True, translate=True, loading='lazy')
    code = fields.Char('Code', required=True)

TemplateCollection()


class AccountTemplateDatev(ModelSQL, ModelView):
    'Account Template Datev'
    _name = 'account.account.template.datev'
    _description = __doc__

    name = fields.Char('Name', required=True, select=1)
    code = fields.Integer('Code', required=True, select=1)
    year = fields.Integer('Year', required=True)
    additional_function = fields.Selection([
            ('KU', 'No VAT'),
            ('AV', 'Input Tax Only'),
            ('AM', 'Output Tax Only')
            ], 'Additional Function')
    tax_automatic = fields.Boolean('Tax Automatic',
            states={
                'invisible': Equal(Eval('additional_function'), 'KU')
                }, depends=['additional_function'])
    posting_block = fields.Boolean('Posting Block',
            states={
                'invisible': Or(Bool(Eval('tax_automatic')),
                        Not(Equal(Eval('additional_function'), 'KU')))
                }, depends=['tax_automatic', 'additional_function'], select=2)
    template_collection = fields.Many2One(
            'account.account.template.datev.collection',
            'Datev Template Collection', required=True)

    def _get_account_datev_value(self, template, account=None):
        '''
        Set the values for account creation.

        :param template: the BrowseRecord of the template
        :param account: the BrowseRecord of the account to update
        :return: a dictionary with account fields as key and values as value
        '''
        res = {}
        if not account or account.name != template.name:
            res['name'] = template.name
        if not account or account.code != template.code:
            res['code'] = template.code
        if not account or account.year != template.year:
            res['year'] = template.year
        if not account or account.additional_function != \
                template.additional_function:
            res['additional_function'] = template.additional_function
        if not account or account.tax_automatic != template.tax_automatic:
            res['tax_automatic'] = template.tax_automatic
        if not account or account.posting_block != template.posting_block:
            res['tax_automatic'] = template.tax_automatic
        if not account or account.tax_function_code != \
                template.tax_function_code:
            res['tax_function_code'] = template.tax_function_code.id
        if not account or account.template.id != template.id:
            res['template'] = template.id
        return res


    def create_account_datev(self, template, company_id,
            template2datevaccount=None):
        account_datev_obj = Pool().get('account.account.datev')
        lang_obj = Pool().get('ir.lang')

        if template2datevaccount is None:
            template2datevaccount = {}

        if isinstance(template, (int, long)):
            template = self.browse(template)

        if template.id not in template2datevaccount:
            vals = self._get_account_datev_value(template)
            vals['company'] = company_id

            new_id = account_datev_obj.create(vals)

            prev_lang = template._context.get('language') or 'en_US'
            for lang in lang_obj.get_translatable_languages():
                if lang == prev_lang:
                    continue
                template.setLang(lang)
                data = {}
                for field in template._columns.keys():
                    if getattr(template._columns[field], 'translate', False):
                        data[field] = template[field]
                if data:
                    with Transaction().set_context(language=lang):
                        account_datev_obj.write(new_id, data)
            template.setLang(prev_lang)
            template2datevaccount[template.id] = new_id
        else:
            new_id = template2datevaccount[template.id]
        return new_id

AccountTemplateDatev()


class AccountDatev(ModelSQL, ModelView):
    'Account Datev'
    _name = 'account.account.datev'
    _description = __doc__

    company = fields.Many2One('company.company', 'Company', required=True)
    name = fields.Char('Name', required=True)
    code = fields.Integer('Code', required=True)
    year = fields.Integer('Year', required=True)
    additional_function = fields.Selection([
            ('KU', 'No VAT'),
            ('AV', 'Input Tax Only'),
            ('AM', 'Output Tax Only')
            ], 'Additional Function',
            on_change=['additional_function'],
            states={
                'required': Bool(Eval('tax_automatic'))
                }, depends=['tax_automatic'])
    tax_automatic = fields.Boolean('Tax Automatic',
            states={
                'invisible': Equal(Eval('additional_function'), 'KU')
                }, on_change=['tax_automatic'],
            depends=['additional_function'])
    posting_block = fields.Boolean('Posting Block',
            states={
                'invisible': Or(Bool(Eval('tax_automatic')),
                        Not(Equal(Eval('additional_function'), 'KU')))
                }, depends=['tax_automatic', 'additional_function'])
    template = fields.Many2One('account.account.template.datev',
            'Account Datev Template')

    def default_posting_block(self):
        return False

    def default_tax_automatic(self):
        return False

    def default_company(self):
        return Transaction().context.get('company', False)

    def on_change_additional_function(self, vals):
        res = {}
        if vals.get('additional_function'):
            if vals['additional_function'] == 'KU':
                res['tax_automatic'] = False
                res['tax_function_code'] = False
            else:
                res['posting_block'] = False
        return res

    def on_change_tax_automatic(self, vals):
        res = {}
        if not vals.get('tax_automatic'):
            res['tax_function_code'] = False
        return res

    def get_rec_name(self, ids, name):
        if not ids:
            return {}
        res = {}
        for account in self.browse(ids):
            res[account.id] = str(account.code) + " - " + account.name
        return res

    def search_rec_name(self, name, clause):
        try:
            operator = clause[1]
            if operator == 'ilike':
                operator = '='
            clause_code = clause[2].strip('%')
            ids = self.search([
                ('code', operator, int(clause_code)),
                ])
            if ids:
                return [('id', 'in', ids)]
        except:
            pass
        return [('name',) + tuple(clause[1:])]

    def update_account_datev(self, account_datev, template2datevaccount=None):
        '''
        Update recursively accounts based on template.

        :param account: an account id or the BrowseRecord of the account
        :param template2account: a dictionary with template id as key
                and account id as value, used to convert template id
                into account. The dictionary is filled with new accounts
        :param template2type: a dictionary with type template id as key
                and type id as value, used to convert type template id
                into type.
        '''
        template_obj = Pool().get('account.account.template.datev')
        lang_obj = Pool().get('ir.lang')

        if template2datevaccount is None:
            template2datevaccount = {}

        if isinstance(account_datev, (int, long)):
            account_datev = self.browse(account_datev)

        if account_datev.template:
            vals = template_obj._get_account_datev_value(
                    account_datev.template,
                    account=account_datev)
            if vals:
                self.write(account_datev.id, vals)

            prev_lang = account_datev._context.get('language') or 'en_US'
            for lang in lang_obj.get_translatable_languages():
                if lang == prev_lang:
                    continue
                account_datev.setLang(lang)
                with Transaction().set_context(language=lang):
                    data = template_obj._get_account_datev_value(
                            account_datev.template, account=account_datev)
                    if data:
                        self.write(account_datev.id, data)
            account_datev.setLang(prev_lang)
            template2datevaccount[account_datev.template.id] = account_datev.id

AccountDatev()


class Account(ModelSQL, ModelView):
    _name = 'account.account'

    def __init__(self):
        super(Account, self).__init__()
        self._constraints += [
            ('check_taxes_datev', 'default_tax_vs_tax_datev'),
        ]
        self._error_messages.update({
            'default_tax_vs_tax_datev': 'The combination of Datev tax '
            'configuration and default tax is not possible!',
            })
        self.fiscalyear = copy.copy(self.fiscalyear)
        if self.fiscalyear.on_change is None:
            self.fiscalyear.on_change = []
        if 'fiscalyear' not in self.fiscalyear.on_change:
            self.fiscalyear.on_change += ['fiscalyear']
        self._rpc.update({
            'on_change_fiscalyear': False,
            })
        self._reset_columns()

    start_year = fields.Function(fields.Integer('Start Year'), 'get_start_year')
    account_datev = fields.Many2One('account.account.datev', 'Account Datev',
            domain=[('year', '=', Eval('start_year'))],
            states={
                'invisible': Equal(Eval('kind'), 'view'),
                'readonly': Bool(Eval('move_lines')),
                }, depends=['start_year', 'kind', 'move_lines'])

    def on_change_fiscalyear(self, vals):
        fiscalyear_obj = Pool().get('account.fiscalyear')
        res = {}
        if vals.get('fiscalyear'):
            fiscalyear = fiscalyear_obj.browse(
                    vals['fiscalyear'])
            res['start_year'] = fiscalyear.start_year
        return res


    def get_start_year(self, ids, name):
        res = {}
        for account in self.browse(ids):
            res[account.id] = account.fiscalyear.start_year
        return res

    def export_datev(self, datev_export, range='changes'):
        export_datev_obj = Pool().get('account.export.datev')
        fiscalyear_obj = Pool().get('account.fiscalyear')
        result = {}
        now = datetime.datetime.now()

        args = [('company', '=', datev_export.company.id),
                ('account_datev', '=', True)]
        if range=='changes':
            args2 = [
                ('date', '<', now),
                ('id', '!=', datev_export.id)
                ]
            export_ids = export_datev_obj.search(args2, limit=1,
                    order=[('date', 'DESC')])
            if export_ids:
                export = export_datev_obj.browse(export_ids[0])

                args = ['AND', ['OR', ['AND', ('write_date', '=', False),
                        ('create_date', '>', export.date)],
                        ('write_date', '>', export.date)],
                        ('company', '=', datev_export.company.id),
                        ('account_datev', '!=', False)]

        account_ids = self.search(args)

        if account_ids:
            res = {}
            for account in self.browse(account_ids):
                vals = self._get_datev_export_values(account)
                export_line = export_datev_obj._create_export_line(
                        _DATEV_ACCOUNT_FIELDS, vals)

                res.setdefault(account.fiscalyear.id, [])
                res[account.fiscalyear.id].append((export_line))

            if res:
                for fiscalyear in fiscalyear_obj.browse(
                        res.keys()):
                    filename = 'Sachkonten ' + fiscalyear.name + ' ' + \
                            datev_export.date.strftime('%Y_%m_%d_%H_%M')
                    result[filename] = StringIO.StringIO()
                    for line in res[fiscalyear.id]:
                        result[filename].write(
                                (line + '\n').encode('windows-1252'))
        return result

    def _get_datev_export_values(self, account):
        res = {}
        res['Kontonummer'] = account.account_datev.code
        res['Kontobeschriftung'] = re.sub(r'[\n\t\v\r\f]', '', account.name)
        return res

    def check_taxes_datev(self, ids):
        res = True
        for account in self.browse(ids):
            if account.account_datev and account.taxes:
                if account.account_datev.tax_automatic:
                    for tax in account.taxes:
                        if tax.tax_function_code.id != \
                                account.account_datev.tax_function_code.id:
                            res = False
                            break
                elif account.account_datev.additional_function:
                    for tax in account.taxes:
                        if tax.tax_function_type != \
                                account.account_datev.additional_function:
                            res = False
                            break
        return res

    def update_account(self, account, template2account=None, template2type=None,
            template2datevaccount=None, previoustemplate2account=None):
        '''
        Update recursively accounts based on template.

        :param account: an account id or the BrowseRecord of the account
        :param template2account: a dictionary with template id as key
                and account id as value, used to convert template id
                into account. The dictionary is filled with new accounts
        :param template2type: a dictionary with type template id as key
                and type id as value, used to convert type template id
                into type.
        '''
        template_obj = Pool().get('account.account.template')
        lang_obj = Pool().get('ir.lang')

        if template2account is None:
            template2account = {}

        if template2type is None:
            template2type = {}

        if template2datevaccount is None:
            template2datevaccount = {}

        if previoustemplate2account is None:
            previoustemplate2account = {}

        if isinstance(account, (int, long)):
            account = self.browse(account)

        if account.template:
            vals = template_obj._get_account_value(
                    account.template, account=account)
            if account.type.id != template2type.get(account.template.type.id,
                    False):
                vals['type'] = template2type.get(account.template.type.id,
                        False)
            if account.account_datev.id != template2datevaccount.get(
                    account.template.account_datev.id, False):
                vals['account_datev'] = \
                        template2datevaccount.get(
                                account.template.account_datev.id, False)
            predecessor_ids = []
            for predecessor in account.template.predecessors:
                if not previoustemplate2account.get(predecessor.id):
                    continue
                predecessor_ids += [previoustemplate2account[predecessor.id]]
            if predecessor_ids:
                vals['predecessors'] = [('set', predecessor_ids)]
            if vals:
                self.write(account.id, vals)

            prev_lang = account._context.get('language') or 'en_US'
            for lang in lang_obj.get_translatable_languages():
                if lang == prev_lang:
                    continue
                account.setLang(lang)
                with Transaction().set_context(language=lang):
                    data = template_obj._get_account_value(
                            account.template, account=account)
                    if data:
                        self.write(account.id, data)
            account.setLang(prev_lang)
            template2account[account.template.id] = account.id

        for child in account.childs:
            self.update_account(child,
                    template2account=template2account,
                    template2type=template2type,
                    template2datevaccount=template2datevaccount,
                    previoustemplate2account=previoustemplate2account)

Account()


class AccountTemplate(ModelSQL, ModelView):
    _name = 'account.account.template'

    account_datev = fields.Many2One('account.account.template.datev',
            'Account Datev')
    account_datev_template_collection = fields.Many2One(
            'account.account.template.datev.collection',
            'Datev Template Collection')

    def create_account(self, template, company_id, template2account=None,
            template2datevaccount=None, template2type=None, parent_id=None,
            previoustemplate2account=None):
        '''
        Create recursively accounts based on template.

        :param template: the template id or the BrowseRecord of template
                used for account creation
        :param company_id: the id of the company for which accounts
                are created
        :param template2account: a dictionary with template id as key
                and account id as value, used to convert template id
                into account. The dictionary is filled with new accounts
        :param template2type: a dictionary with type template id as key
                and type id as value, used to convert type template id
                into type.
        :param parent_id: the account id of the parent of the accounts
                that must be created
        :return: id of the account created
        '''
        account_obj = Pool().get('account.account')
        lang_obj = Pool().get('ir.lang')

        if template2account is None:
            template2account = {}

        if template2type is None:
            template2type = {}

        if template2datevaccount is None:
            template2datevaccount = {}

        if isinstance(template, (int, long)):
            template = self.browse(template)

        if template.id not in template2account:
            vals = self._get_account_value(template)
            vals['company'] = company_id
            vals['parent'] = parent_id
            vals['type'] = template2type.get(template.type.id, False)
            if template.account_datev:
                vals['account_datev'] = \
                        template2datevaccount[template.account_datev.id]

            predecessor_ids = []
            for predecessor in template.predecessors:
                if not previoustemplate2account.get(predecessor.id):
                    continue
                predecessor_ids += [previoustemplate2account[predecessor.id]]
            if predecessor_ids:
                vals['predecessors'] = [('set', predecessor_ids)]

            context = Transaction().context.copy()
            context['no_update_tree'] = True
            with Transaction().set_context(context):
                new_id = account_obj.create(vals)

            prev_lang = template._context.get('language') or 'en_US'
            for lang in lang_obj.get_translatable_languages():
                if lang == prev_lang:
                    continue
                template.setLang(lang)
                data = {}
                for field in template._columns.keys():
                    if getattr(template._columns[field], 'translate', False):
                        data[field] = template[field]
                if data:
                    with Transaction().set_context(language=lang):
                        account_obj.write(new_id, data)
            template.setLang(prev_lang)
            template2account[template.id] = new_id
        else:
            new_id = template2account[template.id]

        new_childs = []
        for child in template.childs:
            new_childs.append(self.create_account(child,
                company_id, template2account=template2account,
                template2datevaccount=template2datevaccount,
                template2type=template2type, parent_id=new_id,
                previoustemplate2account=previoustemplate2account))
        return new_id

AccountTemplate()


class CreateChartAccountFiscalyearDatev(Wizard):
    'Create Chart Account Fiscalyear Datev'
    _name = 'account.account.create_chart_account.fiscalyear.datev'
    _description = __doc__

    states = {
        'init': {
            'result': {
                'type': 'form',
                'object': 'account.account.create_chart_account.init',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('account', 'Ok', 'tryton-ok', True),
                ],
            },
        },
        'account': {
            'result': {
                'type': 'form',
                'object': 'account.account.create_chart_account.account.fiscalyear',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('create_account', 'Create', 'tryton-ok', True),
                ],
            },
        },
        'create_account': {
            'actions': ['_action_create_account'],
            'result': {
                'type': 'form',
                'object': 'account.account.create_chart_account.properties.fiscalyear',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('create_properties', 'Create', 'tryton-ok', True),
                ],
            },
        },
        'create_properties': {
            'result': {
                'type': 'action',
                'action': '_action_create_properties',
                'state': 'end',
            },
        },
    }

    def _action_create_account(self, datas):
        pool = Pool()
        account_type_template_obj = pool.get('account.account.type.template')
        account_template_obj = pool.get('account.account.template')
        tax_code_template_obj = pool.get('account.tax.code.template')
        tax_account_template_obj = pool.get('account.tax.tax.account.template')
        tax_tax_code_template_obj = pool.get('account.tax.tax.code.template')
        tax_template_obj = pool.get('account.tax.template')
        tax_rule_template_obj = pool.get('account.tax.rule.template')
        tax_rule_line_template_obj = pool.get('account.tax.rule.line.template')
        tax_obj = pool.get('account.tax')
        tax_rule_obj = pool.get('account.tax.rule')
        tax_rule_line_obj = pool.get('account.tax.rule.line')
        account_datev_template_obj = pool.get('account.account.template.datev')
        fiscalyear_obj = pool.get('account.fiscalyear')
        account_obj = pool.get('account.account')

        with Transaction().set_context(language='en_US',
                fiscalyear=datas['form']['fiscalyear'], no_update_tree=True):

            account_template = account_template_obj.browse(
                    datas['form']['account_template'])

            # Create account types
            template2type = {}
            account_type_template_obj.create_type(
                    account_template.type, datas['form']['company'],
                    template2type=template2type)

            # Create datev accounts
            template2datevaccount = {}
            account_datev_template_ids = account_datev_template_obj.search([
                ('template_collection', '=',
                         account_template.account_datev_template_collection),
                ])
            for account_datev_template in account_datev_template_obj.browse(
                    account_datev_template_ids):
                account_datev_template_obj.create_account_datev(
                        account_datev_template, datas['form']['company'],
                        template2datevaccount=template2datevaccount)

            previoustemplate2account = {}
            if Transaction().context.get('fiscalyear'):
                fiscalyear = fiscalyear_obj.browse(
                        Transaction().context['fiscalyear'])
                args = [
                    ('end_date', '<', fiscalyear.start_date),
                    ('company', '=', datas['form']['company'])
                    ]
                previous_year_id = fiscalyear_obj.search(args, limit=1,
                        order=[('end_date', 'DESC')])

                if previous_year_id:
                    args = [
                        ('fiscalyear', '=', previous_year_id[0]),
                        ('company', '=', datas['form']['company'])
                        ]
                    previous_account_ids = account_obj.search(args)
                    if previous_account_ids:
                        for previous_account in account_obj.browse(
                                previous_account_ids):
                            if previous_account.template:
                                previoustemplate2account[
                                        previous_account.template.id] = \
                                        previous_account.id

            # Create accounts
            template2account = {}
            account_template_obj.create_account(
                    account_template, datas['form']['company'],
                    template2account=template2account,
                    template2type=template2type,
                    template2datevaccount=template2datevaccount,
                    previoustemplate2account=previoustemplate2account)

            # Update MPTT of accounts once for all
            account_obj._rebuild_tree('parent', False, 0)

            # Create tax codes
            template2tax_code = {}
            tax_code_template_ids = tax_code_template_obj.search([
                ('account', '=', datas['form']['account_template']),
                ('parent', '=', False),
                ])
            for tax_code_template in tax_code_template_obj.browse(
                    tax_code_template_ids):
                tax_code_template_obj.create_tax_code(
                        tax_code_template, datas['form']['company'],
                        template2tax_code=template2tax_code)

            # Update taxes
            template2tax = {}
            tax_ids = tax_obj.search([
                ('company', '=', datas['form']['company']),
                ('parent', '=', False),
                ])
            if tax_ids:
                for tax in tax_obj.browse(tax_ids):
                    tax_obj.update_tax(tax,
                            template2tax_code=template2tax_code,
                            template2account=template2account,
                            template2tax=template2tax)

            # Create missing taxes
            tax_template_ids = tax_template_obj.search([
                ('template_collection', '=',
                        account_template.template_collection.id),
                ('parent', '=', False),
                ])
            for tax_template in tax_template_obj.browse(
                    tax_template_ids):
                tax_template_obj.create_tax(tax_template,
                        datas['form']['company'],
                        template2tax_code=template2tax_code,
                        template2account=template2account,
                        template2tax=template2tax)

            # Update taxes on accounts
            account_template_obj.update_account_taxes(
                    account_template, template2account, template2tax)

            # Create tax account entries
            template2tax_account = {}
            tax_account_template_ids = tax_account_template_obj.search([
                    ('account_template', '=', datas['form']['account_template']),
                ])
            for tax_account_template in tax_account_template_obj.browse(
                    tax_account_template_ids):
                tax_account_template_obj.create_tax_account(
                        tax_account_template, datas['form']['fiscalyear'],
                        template2account=template2account,
                        template2tax=template2tax,
                        template2tax_account=template2tax_account)

            # Create tax code entries
            template2tax_tax_code = {}
            tax_tax_code_template_ids = tax_tax_code_template_obj.search([
                    ('account_template', '=', datas['form']['account_template']),
                ])
            for tax_tax_code_template in tax_tax_code_template_obj.browse(
                    tax_tax_code_template_ids):
                tax_tax_code_template_obj.create_tax_tax_code(
                        tax_tax_code_template,
                        template2tax_code=template2tax_code,
                        template2tax=template2tax,
                        template2tax_tax_code=template2tax_tax_code)

            # Update tax rules
            template2rule = {}
            tax_rule_ids = tax_rule_obj.search([
                ('company', '=', datas['form']['company']),
                ])
            if tax_rule_ids:
                for tax_rule in tax_rule_obj.browse(tax_rule_ids):
                    tax_rule_obj.update_rule(tax_rule,
                            template2rule=template2rule)
            # Create missing tax rules
            tax_rule_template_ids = tax_rule_template_obj.search([
                ('template_collection', '=',
                        account_template.template_collection.id),
                ])
            for tax_rule_template in tax_rule_template_obj.browse(
                    tax_rule_template_ids):
                tax_rule_template_obj.create_rule(
                        tax_rule_template, datas['form']['company'],
                        template2rule=template2rule)

            # Update tax rule lines
            template2rule_line = {}
            tax_rule_line_ids = tax_rule_line_obj.search([
                ('rule.company', '=', datas['form']['company']),
                ])
            if tax_rule_line_ids:
                for tax_rule_line in tax_rule_line_obj.browse(
                        tax_rule_line_ids):
                    tax_rule_line_obj.update_rule_line(tax_rule_line,
                            template2tax, template2rule,
                            template2rule_line=template2rule_line)
            # Create missing tax rule lines
            tax_rule_line_template_ids = tax_rule_line_template_obj.search(
                    [
                        ('rule.template_collection', '=',
                                     account_template.template_collection.id),
                        ])
            for tax_rule_line_template in tax_rule_line_template_obj.browse(
                    tax_rule_line_template_ids):
                tax_rule_line_template_obj.create_rule_line(
                        tax_rule_line_template, template2tax, template2rule,
                        template2rule_line=template2rule_line)

        return {'company': datas['form']['company'],
                'fiscalyear': datas['form']['fiscalyear']}

    def _action_create_properties(self, datas):
        fiscalyear_obj = Pool().get('account.fiscalyear')
        party_obj = Pool().get('party.party')

        vals = {
            'default_account_receivable': datas['form']['account_receivable'],
            'default_account_payable': datas['form']['account_payable']
            }
        fiscalyear_obj.write(datas['form']['fiscalyear'], vals)

        party_ids = party_obj.search([])
        parties = party_obj.browse(party_ids)

        new_accounts = [
            {
                'create_date': datetime.datetime.now(),
                'create_uid': Transaction().user,
                'fiscalyear': datas['form']['fiscalyear'],
                'account': datas['form']['account_receivable'],
                'type': 'receivable',
            },{
                'create_date': datetime.datetime.now(),
                'create_uid': Transaction().user,
                'fiscalyear': datas['form']['fiscalyear'],
                'account': datas['form']['account_payable'],
                'type': 'payable'
            }]

        account_data = []
        for values in new_accounts:
            for party in parties:
                vals = values.copy()
                vals.update({'party': party.id})
                account_data.append(vals)

        if account_data:
            fields2 = [x for x in account_data[0].keys()]
            item_fields = ",".join(x for x in fields2)
            item_fields2 = ",".join('%(' + x + ')s' for x in fields2)
            Transaction().cursor.executemany('INSERT INTO party_account(' +
                    item_fields + ') VALUES (' + item_fields2 + ')',
                    account_data)

        return {}

CreateChartAccountFiscalyearDatev()


class UpdateChartAccountFiscalyearDatev(Wizard):
    'Update Chart Account from Template'
    _name = 'account.account.update_chart_account.fiscalyear.datev'
    _description = __doc__

    states = {
        'init': {
            'result': {
                'type': 'form',
                'object': 'account.account.update_chart_account.init',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('start', 'Ok', 'tryton-ok', True),
                ],
            },
        },
        'start': {
            'actions': ['_action_update_account'],
            'result': {
                'type': 'form',
                'object': 'account.account.update_chart_account.start',
                'state': [
                    ('end', 'Ok', 'tryton-ok', True),
                ],
            },
        },
    }

    def _action_update_account(self, datas):
        pool = Pool()
        account_type_obj = pool.get('account.account.type')
        account_type_template_obj = pool.get('account.account.type.template')
        account_obj = pool.get('account.account')
        account_template_obj = pool.get('account.account.template')
        tax_code_obj = pool.get('account.tax.code')
        tax_code_template_obj = pool.get('account.tax.code.template')
        tax_obj = pool.get('account.tax')
        tax_template_obj = pool.get('account.tax.template')
        tax_rule_obj = pool.get('account.tax.rule')
        tax_rule_template_obj = pool.get('account.tax.rule.template')
        tax_rule_line_obj = pool.get('account.tax.rule.line')
        tax_rule_line_template_obj = pool.get('account.tax.rule.line.template')
        tax_account_obj = pool.get('tax.account')
        tax_account_template_obj = pool.get('account.tax.tax.account.template')
        tax_tax_code_obj = pool.get('tax.code')
        tax_tax_code_template_obj = pool.get('account.tax.tax.code.template')
        fiscalyear_obj = pool.get('account.fiscalyear')
        account_datev_obj = pool.get('account.account.datev')
        account_datev_template_obj = pool.get('account.account.template.datev')

        account = account_obj.browse(datas['form']['account'])
        with Transaction().set_context(fiscalyear=account.fiscalyear.id):
            # Update account types
            template2type = {}
            account_type_obj.update_type(account.type,
                    template2type=template2type)
            # Create missing account types
            if account.type.template:
                account_type_template_obj.create_type(
                        account.type.template, account.company.id,
                        template2type=template2type)

            # Update datev accounts
            template2datevaccount = {}
            account_datev_ids = account_datev_obj.search([
                    ('template.template_collection', '=',
                             account.template.account_datev_template_collection)
                    ])
            for account_datev in account_datev_obj.browse(
                    account_datev_ids):
                account_datev_obj.update_account_datev(account_datev,
                        template2datevaccount=template2datevaccount)

            # Create missing datev accounts
            account_datev_template_ids = account_datev_template_obj.search([
                ('template_collection', '=',
                         account.template.account_datev_template_collection),
                ])
            for account_datev_template in account_datev_template_obj.browse(
                    account_datev_template_ids):
                account_datev_template_obj.create_account_datev(
                        account_datev_template, account.company.id,
                        template2datevaccount=template2datevaccount)

            previoustemplate2account = {}
            args = [
                ('end_date', '<', account.fiscalyear.start_date),
                ('company', '=', account.company.id)
                ]
            previous_year_id = fiscalyear_obj.search(args, limit=1,
                    order=[('end_date', 'DESC')])

            if previous_year_id:
                args = [
                    ('fiscalyear', '=', previous_year_id[0]),
                    ('company', '=', account.company.id)
                    ]
                previous_account_ids = account_obj.search(args)
                if previous_account_ids:
                    for previous_account in account_obj.browse(
                            previous_account_ids):
                        if previous_account.template:
                            previoustemplate2account[
                                    previous_account.template.id] = \
                                    previous_account.id

            # Update accounts
            template2account = {}
            account_obj.update_account(account,
                    template2account=template2account,
                    template2type=template2type,
                    template2datevaccount=template2datevaccount,
                    previoustemplate2account=previoustemplate2account)
            # Create missing accounts
            if account.template:
                account_template_obj.create_account(account.template,
                        account.company.id,
                        template2account=template2account,
                        template2type=template2type,
                        template2datevaccount=template2datevaccount,
                        previoustemplate2account=previoustemplate2account)

            # Update tax codes
            template2tax_code = {}
            tax_code_ids = tax_code_obj.search([
                ('company', '=', account.company.id),
                ('parent', '=', False),
                ])
            for tax_code in tax_code_obj.browse(tax_code_ids):
                tax_code_obj.update_tax_code(tax_code,
                        template2tax_code=template2tax_code)
            # Create missing tax codes
            if account.template:
                tax_code_template_ids = tax_code_template_obj.search([
                    ('account', '=', account.template.id),
                    ('parent', '=', False),
                    ])
                for tax_code_template in tax_code_template_obj.browse(
                        tax_code_template_ids):
                    tax_code_template_obj.create_tax_code(
                            tax_code_template, account.company.id,
                            template2tax_code=template2tax_code)

            # Update taxes
            template2tax = {}
            tax_ids = tax_obj.search([
                ('company', '=', account.company.id),
                ('parent', '=', False),
                ])
            for tax in tax_obj.browse(tax_ids):
                tax_obj.update_tax(tax,
                        template2tax_code=template2tax_code,
                        template2account=template2account,
                        template2tax=template2tax)

            # Create missing taxes
            if account.template:
                tax_template_ids = tax_template_obj.search([
                    ('template_collection', '=',
                        account.template.template_collection),
                    ('parent', '=', False),
                    ])
                for tax_template in tax_template_obj.browse(
                        tax_template_ids):
                    tax_template_obj.create_tax(tax_template,
                            account.company.id,
                            template2tax_code=template2tax_code,
                            template2account=template2account,
                            template2tax=template2tax)

            # Update taxes on accounts
            account_obj.update_account_taxes(account,
                    template2account, template2tax)

            # Update tax account entries
            template2tax_account = {}
            tax_account_ids = tax_account_obj.search([
                ('tax.company', '=', account.company.id),
                ('fiscalyear', '=', account.fiscalyear.id)
                ])
            for tax_account in tax_account_obj.browse(tax_account_ids):
                tax_account_obj.update_tax_account(tax_account,
                        account.fiscalyear.id, template2account=template2account,
                        template2tax=template2tax,
                        template2tax_account=template2tax_account)

            # Create missing tax account entries
            tax_account_template_ids = tax_account_template_obj.search([
                ('account_template', '=', account.template.id),
                ])
            for tax_account_template in tax_account_template_obj.browse(
                    tax_account_template_ids):
                tax_account_template_obj.create_tax_account(
                        tax_account_template, account.fiscalyear.id,
                        template2account=template2account,
                        template2tax=template2tax,
                        template2tax_account=template2tax_account)

            # Update tax tax code entries
            template2tax_tax_code = {}
            tax_tax_code_ids = tax_tax_code_obj.search([
                ('tax.company', '=', account.company.id),
                ['OR', ('valid_from', '<', account.fiscalyear.end_date),
                ('valid_from', '=', False)],
                ['OR', ('valid_to', '>', account.fiscalyear.start_date),
                ('valid_to', '=', False)],
                ])
            for tax_tax_code in tax_tax_code_obj.browse(tax_tax_code_ids):
                tax_tax_code_obj.update_tax_tax_code(tax_tax_code,
                        template2tax_code=template2tax_code,
                        template2tax=template2tax,
                        template2tax_tax_code=template2tax_tax_code)

            # Create missing tax tax code entries
            tax_tax_code_template_ids = tax_tax_code_template_obj.search([
                    ('account_template', '=', account.template.id),
                ])
            for tax_tax_code_template in tax_tax_code_template_obj.browse(
                    tax_tax_code_template_ids):
                tax_tax_code_template_obj.create_tax_tax_code(
                        tax_tax_code_template,
                        template2tax_code=template2tax_code,
                        template2tax=template2tax,
                        template2tax_tax_code=template2tax_tax_code)

            # Update tax rules
            template2rule = {}
            tax_rule_ids = tax_rule_obj.search([
                ('company', '=', account.company.id),
                ])
            for tax_rule in tax_rule_obj.browse(tax_rule_ids):
                tax_rule_obj.update_rule(tax_rule,
                        template2rule=template2rule)
            # Create missing tax rules
            if account.template:
                tax_rule_template_ids = tax_rule_template_obj.search([
                    ('template_collection', '=',
                             account.template.template_collection.id),
                    ])
                for tax_rule_template in tax_rule_template_obj.browse(
                        tax_rule_template_ids):
                    tax_rule_template_obj.create_rule(
                            tax_rule_template, account.company.id,
                            template2rule=template2rule)

            # Update tax rule lines
            template2rule_line = {}
            tax_rule_line_ids = tax_rule_line_obj.search([
                ('rule.company', '=', account.company.id),
                ])
            for tax_rule_line in tax_rule_line_obj.browse(
                    tax_rule_line_ids):
                tax_rule_line_obj.update_rule_line(tax_rule_line,
                        template2tax, template2rule,
                        template2rule_line=template2rule_line)
            # Create missing tax rule lines
            if account.template:
                tax_rule_line_template_ids = tax_rule_line_template_obj.search(
                        [
                            ('rule.template_collection', '=',
                                     account.template.template_collection.id),
                            ])
                for tax_rule_line_template in tax_rule_line_template_obj.browse(
                        tax_rule_line_template_ids):
                    tax_rule_line_template_obj.create_rule_line(
                            tax_rule_line_template, template2tax, template2rule,
                            template2rule_line=template2rule_line)
        return {}

UpdateChartAccountFiscalyearDatev()
