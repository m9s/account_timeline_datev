#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Timeline Datev',
    'name_de_DE': 'Buchhaltung Gültigkeitsdauer Datev',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Accounting module providing Datev functionality
    - Provides the configuration of Datev accounts
    - Provides the possbility to create Datev exports
''',
    'description_de_DE': ''' Buchhaltungsmodul für Datevfunktionalität
    - Ermöglicht die Konfiguration von Datev Konten
    - Ermöglicht die Erstellung von Datev Exporten
''',
    'depends': [
        'account_timeline',
        'account_export',
        'account_move_external_reference',
        'account_tax_recapitulative_statement'
    ],
    'xml': [
        'datev.xml',
        'account.xml',
        'tax.xml',
        'move.xml',
        'party.xml',
        'country.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
