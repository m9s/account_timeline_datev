# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import re
import copy
import StringIO
from decimal import Decimal, ROUND_DOWN, ROUND_UP
from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction
from trytond.modules.account_timeline_datev.datev import _DATEV_GENERAL
from trytond.pool import Pool


_DATEV_FIELDS_FIRST_LINE = True
_DATEV_MOVE_FIELDS = {
    1: {
        # Waehrungskennung
        'name': 'EingegWaehr',
        'type': 'A',
        'length': 3,
        },
    2: {
        # Soll/Haben-Kennzeichen
        'name': 'SollHaben',
        'type': 'A',
        'length': 1,
        'required': True,
        },
    3: {
        # Umsatz (ohne Soll/Haben-Kz)
        'name': 'EingegUmsatz',
        'type': 'C',
        'length': 12,
        'required': True,
        },
    4: {
        # BU-Schluessel
        'name': 'BUFeld',
        'type': 'N',
        'length': 2,
        },
    5: {
        # Gegenkonto (ohne BU-Schluessel)
        'name': 'Gegenkonto',
        'type': 'N',
        'length': 9,
        'required': True,
        },
    6: {
        'name': 'Beleg1',
        'type': 'A',
        'length': 12,
        'cut_action': 'cut_right',
        },
    7: {
        'name': 'Beleg2',
        'type': 'A',
        'length': 12,
        'cut_action': 'cut_right',
        },
    8: {
        'name': 'Belegdatum',
        'type': 'D',
        'length': 8,
        'required': True,
        },
    9: {
        'name': 'KtoNr',
        'type': 'N',
        'length': 9,
        'required': True,
        },
    10: {
        'name': 'Kost1',
        'type': 'A',
        'length': 8,
        },
    11: {
        'name': 'Kost2',
        'type': 'A',
        'length': 8,
        },
    12: {
        'name': 'KostMenge',
        'type': 'N',
        'length': 11,
        },
    13: {
        'name': 'Skonto',
        'type': 'C',
        'length': 10,
        },
    14: {
        'name': 'BuchText',
        'type': 'A',
        'length': 30,
        'cut_action': 'cut_right',
        },
    15: {
        'name': 'EULand_UstID',
        'type': 'A',
        'length': 15,
        },
    16: {
        'name': 'EUSteuersatz',
        'type': 'N',
        'length': 4,
        },
    17: {
        'name': 'WKZBasisUmsatz',
        'type': 'A',
        'length': 3,
        },
    18: {
        'name': 'BasisUmsatz',
        'type': 'N',
        'length': 12,
        },
    19: {
        'name': 'Kurs',
        'type': 'N',
        'length': 10,
        },
    20: {
        'name': 'InformationsArt1',
        'type': 'A',
        'lenght': 20,
        },
    21: {
        'name': 'Informationsinhalt1',
        'type': 'A',
        'lenght': 210,
        },
    }

_PATTERN_REFERENCE = re.compile('[^A-Za-z0-9$&%*+-/]')
_PATTERN_DESCRIPTION = re.compile(
    '[\n\t\v\r\f' + _DATEV_GENERAL['separator'] + ']')


class MoveDatev(ModelSQL, ModelView):
    'Move Datev'
    _name = 'account.move.datev'
    _description = __doc__
    _rec_name = 'description'

    amount = fields.Numeric('Amount', digits=(12, 2), required=True)
    tax_correction_key = fields.Integer('Tax and Correction Key')
    contra_account = fields.Integer('Contra Account', required=True)
    reference1 = fields.Char('Reference 1')
    reference2 = fields.Char('Reference 2')
    date = fields.Date('Date', required=True)
    account = fields.Integer('Account', required=True)
    description = fields.Char('Description')
    vat_code = fields.Char('VAT Code')
    info1_type = fields.Char('Information 1 - Type')
    info1_content = fields.Char('Information 1 - Content')
    move = fields.Many2One('account.move', 'Move', required=True)
    export = fields.Many2One('account.export.datev', 'Export',
            ondelete='RESTRICT')

    def __init__(self):
        super(MoveDatev, self).__init__()
        self._error_messages.update({
            'no_datev_account': 'No datev account found. '
                'Please check the datev account definition.',
            'account_blocked': 'The datev account is blocked for '
                'direct posting. Please check your move!',
            'missing_vat_code': 'VAT Code missing on tax line!',
            'taxes_on_both_sides': 'This move produces a datev move that '
                'needs taxes on both sides. This is not yet possible!',
            'computation_error': 'The datev move for the move with name '
                '%s could not be created because of a computation error. '
                'Please inform your admininistrator.',
            'move_datev_already_exists': 'The datev move for move %s has '
                'already been created.',
            })

    def create_move_datev(self, move):
        move_obj = Pool().get('account.move')
        currency_obj = Pool().get('currency.currency')

        if isinstance(move, (int, long)):
            move = move_obj.browse(move)
        if move.move_datev:
            self.raise_user_error('move_datev_already_exists', error_args=(
                move.name,))

        with Transaction().set_context(effective_date=move.date):
            move = move_obj.browse(move.id)
            currency = move.period.fiscalyear.company.currency

            id2account = {}
            new_lines = {}
            accounts_new = {
                'debit': {},
                'credit': {}
                }
            tax_amounts = Decimal('0.0')
            computed_tax_amounts = Decimal('0.0')

            new_ids = []

            reversal = self._check_reversal(move)
            party = self._get_party(move)
            for line in move.lines:
                with Transaction().set_context(effective_date=move.date):
                    if self._hook_skip_line(line):
                        continue
                if line.debit:
                    pointer = 'debit'
                else:
                    pointer = 'credit'

                taxes = self._get_taxes(line)
                tax_amount_line = self._compute_tax_amount_line(line)
                if tax_amount_line:
                    tax_amounts += abs(tax_amount_line)
                    continue
                account_id = line.account.id
                key = (account_id, line.maturity_date, line.date, taxes)
                new_lines.setdefault(key, {
                        'amount_gross': 0,
                        'amount': 0,
                        'reference1': False,
                        'description': '',
                        'vat_code': False,
                        'reversal': False
                        })
                new_line = new_lines[key]
                new_line['amount'] += (line.debit - line.credit)
                reference1 = (line.external_reference or line.reference or
                    False)
                new_line['reference1'] = reference1
                new_line['description'] += (line.name != reference1 and
                    line.name != '-' and (line.name + ' ') or '')
                new_line['vat_code'] = self._get_vat_code(line)
                id2account[account_id] = line.account

            sum_gross_amounts = {
                'debit': Decimal('0.0'),
                'credit': Decimal('0.0'),
                }
            gross_amounts = {
                'debit': [],
                'credit': [],
                }
            for key in new_lines.keys():
                amount = new_lines[key]['amount']
                if amount < 0 and not reversal or \
                        amount > 0 and reversal:
                    pointer = 'credit'
                    new_lines[key]['side'] = 'credit'
                elif amount > 0 and not reversal or \
                        amount < 0 and reversal:
                    pointer = 'debit'
                    new_lines[key]['side'] = 'debit'

                # Use absolute values from this point
                amount = abs(amount)
                new_lines[key]['amount'] = amount
                taxes = key[3]
                gross_amount, tax_amount = self._compute_amounts(taxes, amount)
                computed_tax_amounts += tax_amount
                new_lines[key]['amount_gross'] = gross_amount
                sum_gross_amounts[pointer] += gross_amount
                gross_amounts[pointer].append(gross_amount)
            for pointer in ['debit', 'credit']:
                sum_gross_amounts[pointer] = currency_obj.round(currency,
                    sum_gross_amounts[pointer])

            diff2key = {'debit': {}, 'credit': {}}
            sums = {'debit': Decimal('0.0'), 'credit': Decimal('0.0')}
            for key in new_lines.keys():
                side = new_lines[key]['side']
                account_id, maturity_date, line_date, taxes = key
                unrounded_amount = new_lines[key]['amount_gross']
                amount = currency_obj.round(currency,
                    unrounded_amount, rounding=ROUND_DOWN)
                diff = unrounded_amount - amount
                diff2key[side].setdefault(diff, [])
                diff2key[side][diff].append(key)
                sums[side] += amount
                accounts_new[side].update({
                    key: {
                        'maturity_date': maturity_date,
                        'date': line_date,
                        'taxes': taxes,
                        'amount': amount,
                        'reference1': new_lines[key]['reference1'],
                        'reference2': maturity_date,
                        'description': new_lines[key]['description'],
                        'party': party,
                        'vat_code': new_lines[key]['vat_code'],
                        'reversal': reversal,
                        }})

            round_action = False
            computed_tax_amounts = currency_obj.round(currency,
                computed_tax_amounts)
            if tax_amounts < computed_tax_amounts:
                round_action = 'DOWN'
            elif tax_amounts > computed_tax_amounts:
                round_action = 'UP'

            adjust_both = False
            if (round_action and sum_gross_amounts['debit'] !=
                    sum_gross_amounts['credit']):
                if round_action == 'DOWN':
                    adjust_both = (sum_gross_amounts['debit'] if
                        sum_gross_amounts['debit'] <
                        sum_gross_amounts['credit'] else
                        sum_gross_amounts['credit'])
                elif round_action == 'UP':
                    adjust_both = (sum_gross_amounts['debit'] if
                        sum_gross_amounts['debit'] >
                        sum_gross_amounts['credit'] else
                        sum_gross_amounts['credit'])

            for side in ['debit', 'credit']:
                if sums[side] != sum_gross_amounts[side] or adjust_both:
                    nsum = sums[side]
                    check_sum = (adjust_both if adjust_both else
                        sum_gross_amounts[side])
                    diffs = diff2key[side].keys()
                    diffs.sort(reverse=True)
                    for diff in diffs:
                        keys = diff2key[side][diff]
                        for key in keys:
                            if nsum == check_sum:
                                break
                            accounts_new[side][key]['amount'] = \
                                currency_obj.round(currency,
                                    accounts_new[side][key]['amount'] + diff,
                                    rounding=ROUND_UP)
                            nsum += currency.rounding
                            if nsum == check_sum:
                                break
                        if nsum == check_sum:
                            break

            amount_used = {}
            amount_to_use = {}
            for debit_key in accounts_new['debit'].keys():
                debit_account_id, _, _, debit_taxes = debit_key
                debit_account = id2account[debit_account_id]
                for credit_key in accounts_new['credit'].keys():
                    credit_account_id, _, _, credit_taxes = credit_key
                    credit_account = id2account[credit_account_id]
                    if (debit_taxes and credit_taxes and
                        debit_taxes != credit_taxes):
                        continue

                    debit_account_data = accounts_new['debit'][debit_key]
                    credit_account_data = accounts_new['credit'][credit_key]

                    amount_debit_record = accounts_new['debit'][debit_key]['amount']
                    amount_to_use.setdefault(debit_key,
                        abs(amount_debit_record))
                    amount_credit_record = accounts_new['credit'][credit_key]['amount']
                    amount_to_use.setdefault(credit_key,
                        abs(amount_credit_record))

                    if amount_used.get(debit_key):
                        amount_debit_record -= amount_used[debit_key]
                    else:
                        amount_used.setdefault(debit_key, 0)
                    if amount_used.get(credit_key):
                        amount_credit_record -= amount_used[credit_key]
                    else:
                        amount_used.setdefault(credit_key, 0)

                    amount = min([abs(amount_debit_record),
                        abs(amount_credit_record)])
                    if not amount:
                        continue
                    amount_used[debit_key] += amount
                    amount_used[credit_key] += amount

                    amount = currency_obj.round(currency,
                        amount)
                    amount = abs(amount)
                    if (debit_account_data['reversal'] == True or
                            credit_account_data['reversal'] == True):
                        amount *= -1

                    debit_party_id = (debit_account_data.get('party') and
                        debit_account_data['party'].id or False)
                    datev_account_debit = self._get_account_datev_from_account(
                        id2account[debit_account.id], party_id=debit_party_id)

                    credit_party_id = (credit_account_data.get('party') and
                        credit_account_data['party'].id or False)
                    datev_account_credit = self._get_account_datev_from_account(
                        id2account[credit_account.id], party_id=credit_party_id)
                    if (debit_account_data['taxes'] and
                            credit_account_data['taxes']):
                        if not (debit_account_data['taxes'] ==
                                credit_account_data['taxes']):
                            self.raise_user_error('taxes_on_both_sides')
                    tax_check_account = False
                    if debit_account_data['taxes']:
                        tax_check_account = id2account[debit_account.id]
                    elif credit_account_data['taxes']:
                        tax_check_account = id2account[credit_account.id]
                    taxes = (debit_account_data['taxes'] or
                        credit_account_data['taxes'])
                    datev_tax = self._get_datev_tax(taxes)
                    tax_key = self._get_tax_key(datev_tax, amount,
                        tax_check_account)
                    vat_fact_13b = self._get_vat_fact_13b(datev_tax)
                    if not taxes and (debit_account_data['reversal'] == True or
                            credit_account_data['reversal'] == True):
                        tax_key += 20
                    if not taxes and (
                            credit_account.account_datev.tax_automatic or
                            debit_account.account_datev.tax_automatic):
                        tax_key += 40
                        if (debit_account_data['reversal'] == True or
                                credit_account_data['reversal'] == True):
                            tax_key += 20
                    contra_account = (amount > 0 and
                            datev_account_debit or datev_account_credit)
                    account = (amount > 0 and
                            datev_account_credit or datev_account_debit)
                    reference2 = (debit_account_data['reference2'] or
                        credit_account_data['reference2'] or False)
                    reference1 = (debit_account_data['reference1'] or
                        credit_account_data['reference1'] or False)
                    date = (debit_account_data['date'] or
                        credit_account_data['date'])
                    description = (debit_account_data['description'] or
                        credit_account_data['description'] or False)
                    vat_code = False
                    if datev_tax and datev_tax.vat_code_required:
                        vat_code = (debit_account_data['vat_code'] or
                            credit_account_data['vat_code'])
                        if not vat_code:
                            self.raise_user_error('missing_vat_code')

                    if reference1:
                        reference1 = _PATTERN_REFERENCE.sub('', reference1)
                    if reference2:
                        try:
                            reference2 = reference2.strftime('%d%m%y')
                        except:
                            reference2 = _PATTERN_REFERENCE.sub('', reference2)
                    if description:
                        description = _PATTERN_DESCRIPTION.sub('', description)

                    vals = {
                        'amount': abs(amount),
                        'tax_correction_key': tax_key,
                        'contra_account': contra_account,
                        'reference1': reference1,
                        'reference2': reference2,
                        'date': date,
                        'account': account,
                        'description': (description != reference1 and
                                description != '-' and description or False),
                        'vat_code': vat_code,
                        'info1_type': 'D_L+L-Art' if vat_fact_13b else False,
                        'info1_content': vat_fact_13b,
                        'move': move.id
                        }
                    new_id = self.create(vals)
                    if new_id:
                        new_ids.append(new_id)
            for key in amount_to_use.keys():
                if (key not in amount_used or
                        amount_used.get(key) != amount_to_use[key]):
                    self.raise_user_error('computation_error',
                        error_args=(move.name))
        return new_ids

    def _check_reversal(self, move):
        line = move.lines[0]
        if (line.debit or line.credit) < 0:
            return True
        return False

    def _get_party(self, move):
        return move.lines[0].party or False

    def _compute_tax_amount_line(self, line):
        res = Decimal('0.0')
        for tax_line in line.tax_lines:
            tax = tax_line.tax
            if tax.vat_datev:
                if line.account in (tax.invoice_account,
                        tax.credit_note_account):
                    res = line.debit - line.credit
        return res

    def _compute_amounts(self, taxes, amount):
        tax_obj = Pool().get('account.tax')

        tax_amount = Decimal('0.0')
        gross_amount = Decimal('0.0')

        if not taxes:
            taxes = []
        taxes = tax_obj.browse([x for x in taxes])

        if amount < 0:
            sign = -1
        else:
            sign = 1

        line_processed = False
        for tax in taxes:
            if tax.vat_datev:
                tax_values = tax_obj._process_tax(tax, amount)
                tax_amount = tax_values['amount']
                if tax.reverse_charge:
                    gross_amount = amount
                else:
                    gross_amount = amount + tax_amount
                line_processed = True
                #Break because line must have only one tax_line with
                break
        if not line_processed:
            gross_amount = amount
        tax_amount = abs(tax_amount) * sign
        gross_amount = abs(gross_amount) * sign
        return gross_amount, tax_amount

    def _get_taxes(self, line):
        res = False
        if line.tax_lines:
            res = []
            for tax_line in line.tax_lines:
                res.append(tax_line.tax.id)
            res = tuple(res)
        return res

    def _get_account_datev_from_account(self, account, party_id=None):
        account_obj = Pool().get('account.account')
        party_obj = Pool().get('party.party')

        if isinstance(account, (int, long)):
            account = account_obj.browse(account)
        if not account.account_datev:
            self.raise_user_error('no_datev_account')
        if account.account_datev.posting_block:
            self.raise_user_error('account_blocked')
        res = account.account_datev.code
        if party_id:
            party = party_obj.browse(party_id)

            if account == party.account_receivable:
                if not party.account_receivable_datev:
                    res = self._set_account_datev_party(party.id,
                            'datev.debitor')
                else:
                    res = party.account_receivable_datev
            elif account == party.account_payable:
                if not party.account_payable_datev:
                    res = self._set_account_datev_party(party.id,
                            'datev.creditor')
                else:
                    res = party.account_payable_datev
        return res

    def _set_account_datev_party(self, party_id, type):
        sequence_obj = Pool().get('ir.sequence')
        party_obj = Pool().get('party.party')

        res = sequence_obj.get(type)
        data = {}
        if type == 'datev.debitor':
            data = {'account_receivable_datev': res}
        elif type == 'datev.creditor':
            data = {'account_payable_datev': res}
        party_obj.write(party_id, data)
        return res

    def _get_datev_tax(self, taxes=None):
        tax_obj = Pool().get('account.tax')
        res = False
        if taxes:
            if isinstance(taxes, tuple):
                taxes = list(taxes)
            for tax in tax_obj.browse(taxes):
                if not tax.vat_datev:
                    continue
                res = tax
                break
        return res

    def _get_tax_key(self, tax, amount, account):
        tax_obj = Pool().get('account.tax')

        res = False
        if not tax or not amount or not account:
            return res

        if isinstance(tax, (int, long)):
            tax = tax_obj.browse(tax)

        automatic_account = account.account_datev.tax_automatic
        if not tax.vat_datev:
            return res
        if tax and not automatic_account:
            res = tax.tax_key
            if amount > 0:
                res += tax.correction_key * 10
            elif amount < 0:
                res += tax.reverse_correction_key * 10
        elif not tax and automatic_account and amount > 0:
            res = 40
        elif amount < 0:
            if not tax and automatic_account:
                res = 80
            else:
                res = 20

        return res

    def _get_vat_fact_13b(self, tax):
        tax_obj = Pool().get('account.tax')

        res = False
        if not tax:
            return res
        if isinstance(tax, (int, long)):
            tax = tax_obj.browse(tax)
        if tax.vat_fact_13b:
            if tax.childs:
                percentage = abs(tax.childs[0].percentage)
            else:
                percentage = abs(tax.percentage)
            res = tax.vat_fact_13b + '_' + str(percentage) + '%'
        return res

    def _get_vat_code(self, move_line):
        res = False
        for line in move_line.tax_lines:
            if line.vat_code:
                res = line.vat_code
        return res

    def export_datev(self, datev_export, period_ids, journal_ids,
                range='changes'):
        pool = Pool()
        move_obj = pool.get('account.move')
        move_datev_obj = pool.get('account.move.datev')
        period_obj = pool.get('account.period')
        export_datev_obj = pool.get('account.export.datev')

        move_args = self._get_args_moves_export(datev_export, period_ids,
            journal_ids)
        move_ids = move_obj.search(move_args)

        move_ids_post = move_obj.search([
            ('state', '=', 'draft'),
            ('id', 'in', move_ids)
            ])
        if move_ids_post:
            move_obj.post(move_ids_post)

        args = [
            ('move', 'in', move_ids)
            ]
        if range == 'changes':
            args.append(('export', '=', False))
        move_datev_ids = move_datev_obj.search(args)

        files = {}
        res = {}
        if move_datev_ids:
            moves_datev_processed = []
            for move in move_datev_obj.browse(move_datev_ids):
                vals = self._get_datev_export_values(move)
                export_line = export_datev_obj._create_export_line(
                        _DATEV_MOVE_FIELDS, vals)

                res.setdefault(move.move.period.id, [])
                res[move.move.period.id] += [(export_line)]
                moves_datev_processed.append(move.id)

            vals = {'export': datev_export.id}
            move_datev_obj.write(moves_datev_processed, vals)

            for period in period_obj.browse(res.keys()):
                filename = 'Buchungen ' + period.name
                files[filename] = StringIO.StringIO()
                if _DATEV_FIELDS_FIRST_LINE:
                    files[filename].write(_DATEV_GENERAL['separator'].join(
                        [x['name'] for x in _DATEV_MOVE_FIELDS.values()]))
                    files[filename].write('\n')
                for line in res[period.id]:
                    files[filename].write((line + '\n').encode('windows-1252'))
        return files

    def _get_args_moves_export(self, datev_export, period_ids, journal_ids):
        move_line_obj = Pool().get('account.move.line')
        payable_receivable_ids = self._get_payable_receivable_account_ids()
        move_line_vals = move_line_obj.search_read([
            ('move.journal.type', '=', 'situation'),
            ('account', 'in', payable_receivable_ids),
            ], fields_names=['move'])
        opening_balance_pay_recv_move_ids = [x['move'] for x in move_line_vals]

        args = []
        if period_ids:
            args.append(('period', 'in', period_ids))
        if journal_ids:
            args.append(('journal', 'in', journal_ids))
        if not datev_export.include_opening_balance_moves:
            args.append(('journal.type', '!=', 'situation'))
        else:
            args.extend([['OR',
                ('journal.type', '!=', 'situation'),
                # We do not export opening balance moves of
                # payable and receivable accounts as they are
                # handled by datev itself.
                ['AND',
                    ('journal.type', '=', 'situation'),
                    ('id', 'not in', opening_balance_pay_recv_move_ids)
                ]
            ]])
        return args

    def _get_payable_receivable_account_ids(self):
        fiscalyear_obj = Pool().get('account.fiscalyear')

        res = []
        fiscalyear_ids = fiscalyear_obj.search([])
        for fiscalyear in fiscalyear_obj.browse(fiscalyear_ids):
            res.append(fiscalyear.default_account_receivable.id)
            res.append(fiscalyear.default_account_payable.id)
        return res

    def _get_datev_export_values(self, move):
        res = {}
        res['SollHaben'] = '-'
        res['EingegUmsatz'] = move.amount
        res['Gegenkonto'] = move.contra_account
        res['Belegdatum'] = move.date
        res['KtoNr'] = move.account

        if move.tax_correction_key:
            res['BUFeld'] = move.tax_correction_key
        if move.reference1:
            res['Beleg1'] = _PATTERN_REFERENCE.sub('', move.reference1)
        if move.reference2:
            res['Beleg2'] = _PATTERN_REFERENCE.sub('', move.reference2)
        if move.description:
            res['BuchText'] = _PATTERN_DESCRIPTION.sub('',
                move.description)
        if move.vat_code:
            res['EULand_UstID'] = _PATTERN_REFERENCE.sub('', move.vat_code)
        if move.info1_type:
            res['InformationsArt1'] = move.info1_type
        if move.info1_content:
            res['Informationsinhalt1'] = move.info1_content
        return res

    def _hook_skip_line(self, line):
        '''
        Provide an extensible hook to skip creation of datev moves
        '''
        return False

MoveDatev()


class Move(ModelSQL, ModelView):
    _name = 'account.move'

    move_datev = fields.One2Many('account.move.datev', 'move',
            'Moves Datev', readonly=True)

    def post(self, ids):
        move_datev_obj = Pool().get('account.move.datev')
        if isinstance(ids, (int, long)):
            ids = [ids]
        for move_id in ids:
            move_datev_obj.create_move_datev(move_id)
        return super(Move, self).post(ids)

    def draft(self, ids):
        res = super(Move, self).draft(ids)
        vals = {
            'move_datev': [('delete_all',)]
            }
        return self.write(ids, vals)

    def copy(self, ids, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['move_datev'] = False
        return super(Move, self).copy(ids, default=default)

Move()


class Line(ModelSQL, ModelView):
    _name = 'account.move.line'

    def __init__(self):
        super(Line, self).__init__()

        self._constraints += [
            ('check_tax_datev_lines', 'only_one_tax_datev_line'),
            ('check_account_datev_vs_tax_line', 'tax_not_allowed'),
        ]
        self._error_messages.update({
            'only_one_tax_datev_line': 'There can be only one tax line ' \
                    'with a datev tax automatic!',
            'tax_not_allowed': 'The selected tax of the tax line ' \
                    'is not allowed for this account!',
            })

        self.account = copy.copy(self.account)
        if self.account.domain is None:
            self.account.domain = []
        if ('account_datev', '!=', False) not in self.account.domain:
            self.account.domain += [('account_datev', '!=', False)]
        self._reset_columns()

    def check_tax_datev_lines(self, ids):
        res = True
        for line in self.browse(ids):
            count = 0
            for tax_line in line.tax_lines:
                if tax_line.tax and tax_line.tax.vat_datev:
                    count += 1
                if count > 1:
                    res = False
                    break
        return res

    def check_account_datev_vs_tax_line(self, ids):
        res = True
        for line in self.browse(ids):
            with Transaction().set_context(effective_date=line.date):
                line = self.browse(line.id)
                for tax_line in line.tax_lines:
                    if line.account and line.account.account_datev:
                        if line.account.account_datev.tax_automatic:
                            if tax_line.tax and tax_line.tax.vat_datev and \
                                    tax_line.tax.tax_function_code != \
                                    line.account.account_datev.tax_function_code:
                                res = False
                                break
                        elif tax_line.tax and tax_line.tax.vat_datev and \
                                line.account.account_datev.additional_function and \
                                tax_line.tax.tax_function_type != \
                                line.account.account_datev.additional_function and \
                                line.account != tax_line.tax.invoice_account and \
                                line.account != tax_line.tax.credit_note_account:
                            res = False
                            break
        return res

Line()
