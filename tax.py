# -*- coding: utf-8 -*-
# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, Not, Bool

_VAT_FACTS_13B = [
    ('1', 'Services received from company located in third country.'),
    ('2', 'Delivery of items assigned by way of collateral.'),
    ('3', 'Sales subject to real estate transfer tax.'),
    ('4', 'Construction works done by a company located inland.'),
    ('5', 'Delivery of gas and electricity by a company located in third '
        'country.'),
    ('7', 'Services taxable inland provided by a company located in other '
        'EU-Countries.'),
    ('8', 'Greenhouse gas certificates'),
    ('9', 'Scrap metal'),
    ('10', 'Building cleaning'),
    ('11', 'Delivery of gold.'),
    ]

class TaxFunction(ModelSQL, ModelView):
    'Datev Tax Function'
    _name = 'account.tax.datev.function'
    _description = __doc__

    name = fields.Char('Name', required=True)
    code = fields.Char('Code', required=True)
    type = fields.Selection([
            ('AV', 'Automatic Input Tax'),
            ('AM', 'Automatic Output Tax'),
            ], 'Type', required=True)
    description = fields.Text('Description')

    def get_rec_name(self, ids, name):
        res = {}
        for tax_function in self.browse(ids):
            res[tax_function.id] = tax_function.type + str(tax_function.code)
        return res

TaxFunction()


class Tax(ModelSQL, ModelView):
    _name = 'account.tax'

    vat_datev = fields.Boolean('VAT Datev',
            on_change=['vat_datev'])
    has_tax_key = fields.Boolean('Tax Key Datev',
            states={
                'invisible': Not(Bool(Eval('vat_datev')))
                }, depends=['vat_datev'])
    tax_key = fields.Integer('Tax Key',
            states={
                'invisible': Not(Bool(Eval('has_tax_key')))
                }, depends=['has_tax_key'])
    correction_key = fields.Integer('Correction Key',
            states={
                'invisible': Not(Bool(Eval('has_tax_key')))
                }, depends=['has_tax_key'])
    reverse_correction_key = fields.Integer(
            'Reversing Correction Key',
            states={
                'invisible': Not(Bool(Eval('has_tax_key')))
                }, depends=['has_tax_key'])
    tax_function_type = fields.Selection([
            ('AV', 'Automatic Input Tax'),
            ('AM', 'Automatic Output Tax'),
            ], 'Tax Function Type',
            states={
                'required': Bool(Eval('vat_datev')),
                'invisible': Not(Bool(Eval('vat_datev')))
                }, depends=['vat_datev'])
    tax_function_code = fields.Many2One('account.tax.datev.function',
            'Tax Function Code',
            domain = [('type', '=', Eval('tax_function_type'))],
            states={
                'required': Bool(Eval('vat_datev')),
                'invisible': Not(Bool(Eval('vat_datev')))
                }, depends=['tax_function_type', 'vat_datev'])
    vat_fact_13b = fields.Selection(_VAT_FACTS_13B, u'VAT Fact §13b UStG',
        help=u'Fact for tax resulting from §13b UStG',
        states={
            'invisible': Not(Bool(Eval('vat_datev')))
            }, depends=['vat_datev'])

    def default_vat_datev(self):
        return False

    def default_has_tax_key(self):
        return False

    def on_change_vat_datev(self, vals):
        res = {}
        if not vals.get('vat_datev'):
            res['has_tax_key'] = False
        return res

Tax()


class TaxTemplate(ModelSQL, ModelView):
    _name = 'account.tax.template'

    vat_datev = fields.Boolean('VAT Datev',
            on_change=['vat_datev'])
    has_tax_key = fields.Boolean('Tax Key Datev',
            states={
                'invisible': Not(Bool(Eval('vat_datev')))
                }, depends=['vat_datev'])
    tax_key = fields.Integer('Tax Key',
            states={
                'invisible': Not(Bool(Eval('has_tax_key')))
                }, depends=['has_tax_key'])
    correction_key = fields.Integer('Correction Key',
            states={
                'invisible': Not(Bool(Eval('has_tax_key')))
                }, depends=['has_tax_key'])
    reverse_correction_key = fields.Integer(
            'Reversing Correction Key',
            states={
                'invisible': Not(Bool(Eval('has_tax_key')))
                }, depends=['has_tax_key'])
    tax_function_type = fields.Selection([
            ('AV', 'Automatic Input Tax'),
            ('AM', 'Automatic Output Tax'),
            ], 'Tax Function Type',
            states={
                'required': Bool(Eval('vat_datev')),
                'invisible': Not(Bool(Eval('vat_datev')))
                }, depends=['vat_datev'])
    tax_function_code = fields.Many2One('account.tax.datev.function',
            'Tax Function Code',
            domain = [('type', '=', Eval('tax_function_type'))],
            states={
                'required': Bool(Eval('vat_datev')),
                'invisible': Not(Bool(Eval('vat_datev')))
                }, depends=['tax_function_type', 'vat_datev'])

    def default_vat_datev(self):
        return False

    def default_has_tax_key(self):
        return False

    def on_change_vat_datev(self, vals):
        res = {}
        if not vals.get('vat_datev'):
            res['has_tax_key'] = False
        return res

    def _get_tax_value(self, template,
            tax=None):
        '''
        Set values for tax creation.

        :param template: the BrowseRecord of the template
        :param tax: the BrowseRecord of the tax to update
        :return: a dictionary with account fields as key and values as value
        '''
        res = super(TaxTemplate, self)._get_tax_value(template, tax=tax)
        for field in ('vat_datev', 'has_tax_key', 'tax_key', 'correction_key',
                'reverse_correction_key', 'tax_function_type'):
            if not tax or tax[field] != template[field]:
                res[field] = template[field]
        for field in ('tax_function_code',):
            if not tax or tax[field].id != template[field].id:
                res[field] = template[field].id
        return res

TaxTemplate()


class AccountTemplateDatev(ModelSQL, ModelView):
    _name = 'account.account.template.datev'

    tax_function_code = fields.Many2One('account.tax.datev.function',
            'Tax Function Code',
            domain = [('type', '=', Eval('additional_function'))],
            states={
                'required': Bool(Eval('tax_automatic')),
                'invisible': Not(Bool(Eval('tax_automatic')))
                }, depends=['tax_automatic', 'additional_function'])

AccountTemplateDatev()


class AccountDatev(ModelSQL, ModelView):
    _name = 'account.account.datev'

    tax_function_code = fields.Many2One('account.tax.datev.function',
            'Tax Function Code',
            domain = [('type', '=', Eval('additional_function'))],
            states={
                'required': Bool(Eval('tax_automatic')),
                'invisible': Not(Bool(Eval('tax_automatic')))
                }, depends=['tax_automatic', 'additional_function'])

AccountDatev()
