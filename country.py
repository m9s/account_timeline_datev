# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class Country(ModelSQL, ModelView):
    _name = 'country.country'

    datev_code = fields.Char('Datev Code', select=2,
           help='The Datev country code.')

Country()
