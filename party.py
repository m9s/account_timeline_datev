# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import re
import datetime
import StringIO
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, Not, Bool
from trytond.pool import Pool


_DATEV_PARTY_FIELDS = {
    1: {
        'name': 'Kontonummer',
        'type': 'N',
        'length': 9,
        'required': True
        },
    2: {
        'name': 'Name 1',
        'type': 'A',
        'length': 40,
        'cut_action': 'cut_right'
        },
    3: {
        'name': 'Name 2',
        'type': 'A',
        'length': 40,
        'cut_action': 'cut_right'
        },
    4: {
        'name': 'Kurzbezeichnung',
        'type': 'A',
        'length': 15,
        'cut_action': 'cut_right',
        },
    5: {
        #USt-IdNr.
        'name': 'UStID',
        'type': 'A',
        'length': 15,
        },
    6: {
        'name': 'Strasse',
        'type': 'A',
        'length': 36,
        'cut_action': 'cut_street'
        },
    7: {
        'name': 'Postfach',
        'type': 'A',
        'length': 10,
        },
    8: {
        'name': 'Postleitzahl',
        'type': 'A',
        'length': 10,
        },
    9: {
        'name': 'Ort',
        'type': 'A',
        'length': 30,
        'cut_action': 'cut_right'
        },
    10: {
        #Nat.-Kennzeichen
        'name': 'Nat-Kennzeichen',
        'type': 'A',
        'length': 3,
        },
    11: {
        'name': 'Anrede-Schluessel',
        'type': 'N',
        'length': 1,
        },
    12: {
        # Anrede individuell
        'name': 'Anrede',
        'type': 'A',
        'length': 40,
        'cut_action': 'cut_right',
        },
    13: {
        # Titel/Akad. Grad
        'name': 'Titel',
        'type': 'A',
        'length': 25,
        },
    14: {
        'name': 'Zustellzusatz',
        'type': 'A',
        'lenght': 35,
        'cut_action': 'cut_right'
        },
    15: {
        'name': 'Kundennummer',
        'type': 'A',
        'length': 15,
        },
    16: {
        'name': 'Sprache',
        'type': 'N',
        'lenght': 2,
        },
    17: {
        'name': 'Telefon',
        'type': 'A',
        'length': 20
        },
    18: {
        'name': 'Telefax',
        'type': 'A',
        'length': 20,
        },
    19: {
        'name': 'Mobiltelefon',
        'type': 'A',
        'length': 20,
        },
    20: {
        'name': 'E-Mail',
        'type': 'A',
        'length': 60,
        },
    21: {
        'name': 'Internet URL',
        'type': 'A',
        'length': 60,
        },
    22: {
        'name': 'Ansprechpartner',
        'type': 'A',
        'lenght': 30,
        'cut_action': 'cut_right'
        },
    23: {
        'name': 'Vertreter',
        'type': 'A',
        'length': 40,
        'cut_action': 'cut_right'
        },
    }


class Party(ModelSQL, ModelView):
    _name = 'party.party'

    account_payable_datev = fields.Property(fields.Char(
            'Account Payable Datev',
            domain=[('company', '=', Eval('company', 0))],
            states={
                'invisible': Not(Bool(Eval('company'))),
            }, depends=['company']))
    account_receivable_datev = fields.Property(fields.Char(
            'Account Receivable Datev',
            domain=[('company', '=', Eval('company', 0))],
            states={
                'invisible': Not(Bool(Eval('company'))),
            }, depends=['company']))
    address_datev = fields.Function(fields.Many2One('party.address',
            'Address Datev'), 'get_address_datev')

    def get_address_datev(self, ids, name):
        address_obj = Pool().get('party.address')
        if not ids:
            return []
        res = {}

        type = None
        if 'address_datev' in name:
            type = 'invoice'

        for party_id in ids:
            res[party_id] = False
            args = [
                (type, '=', True),
                ('party', '=', party_id)
                ]
            address_ids = address_obj.search(args, limit=1)
            if address_ids:
                res[party_id] = address_ids[0]
            else:
                party = self.browse(party_id)
                if party.addresses:
                    res[party.id] = party.addresses[0].id
        return res

    def export_datev(self, datev_export, range='changes'):
        export_datev_obj = Pool().get('account.export.datev')
        result = {}
        now = datetime.datetime.now()

        args = []
        if range=='changes':
            args2 = [
                ('date', '<', now),
                ('id', '!=', datev_export.id)
                ]
            export_ids = export_datev_obj.search(args2, limit=1,
                    order=[('date', 'DESC')])
            if export_ids:
                export = export_datev_obj.browse(export_ids[0])
                args = ['OR', ['AND', ('write_date', '=', False),
                        ('create_date', '>', export.date)],
                        ('write_date', '>', export.date)]

        party_ids = self.search(args)
        if party_ids:
            res = []
            for party in self.browse(party_ids):
                if party.account_receivable_datev or party.account_payable_datev:
                    vals = self._get_datev_export_values(party)
                    if party.account_receivable_datev:
                        vals2 = vals.copy()
                        try:
                            account_number = int(party.account_receivable_datev)
                        except:
                            account_number = party.account_receivable_datev
                        vals2['Kontonummer'] = account_number
                        export_line = export_datev_obj._create_export_line(
                                _DATEV_PARTY_FIELDS, vals2)
                        res.append(export_line)
                    if party.account_payable_datev:
                        vals2 = vals.copy()
                        try:
                            account_number = int(party.account_payable_datev)
                        except:
                            account_number = party.account_payable_datev
                        vals2['Kontonummer'] = account_number
                        export_line = export_datev_obj._create_export_line(
                                _DATEV_PARTY_FIELDS, vals2)
                        res.append(export_line)
            if res:
                filename = 'Personenkonten ' + \
                        datev_export.date.strftime('%Y_%m_%d_%H_%M')
                result[filename] = StringIO.StringIO()
                for line in res:
                    result[filename].write((line + '\n').encode('windows-1252'))
        return result

    def _get_datev_export_values(self, party):
        pattern = re.compile(r'[\n\t\v\r\f]')
        res = {}
        res['Name 1'] = pattern.sub('', party.name)
        res['Kurzbezeichnung'] = pattern.sub('', party.name)
        if party.vat_country and party.vat_code:
            res['UStID'] = pattern.sub('', party.vat_code)
        if party.address_datev:
            if party.address_datev.street:
                res['Strasse'] = pattern.sub('', party.address_datev.street)
                if party.address_datev.streetbis:
                    res['Strasse'] += '/ '
            if party.address_datev.streetbis:
                res.setdefault('Strasse', '')
                res['Strasse'] += pattern.sub('', party.address_datev.streetbis)
            if party.address_datev.zip:
                res['Postleitzahl'] = pattern.sub('', party.address_datev.zip)
            if party.address_datev.city:
                res['Ort'] = pattern.sub('', party.address_datev.city)
            if party.address_datev.country and \
                    party.address_datev.country.datev_code:
                res['Nat-Kennzeichen'] = party.address_datev.country.datev_code
        return res

    def copy(self, ids, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['account_payable_datev'] = False
        default['account_receivable_datev'] = False
        return super(Party, self).copy(ids, default=default)

Party()
