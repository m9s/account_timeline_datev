# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class FiscalYear(ModelSQL, ModelView):
    _name = 'account.fiscalyear'

    start_year = fields.Function(fields.Integer('Start Year'), 'get_start_year')

    def get_start_year(self, ids, name):
        res = {}
        for fiscalyear in self.browse(ids):
            res[fiscalyear.id] = int(fiscalyear.start_date.strftime('%Y'))
        return res

FiscalYear()
